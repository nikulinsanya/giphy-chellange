export default class Image {
  url: string;
  id: string;
  images: {
    downsized: {
      height: number;
      size: number;
      url: string;
      width: number;
    };
    original: {
      height: number;
      size: number;
      url: string;
      width: number;
    }
  };
}
