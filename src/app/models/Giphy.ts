import Pagination from './Pagination';
import Image from './Image';

export default class Giphy {
  data: Array<Image>;
  pagination: Pagination;
}
