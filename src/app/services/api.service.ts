import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly API_KEY = 'CdRKiCMbTnt9CkZTZ0lGukSczk6iT4Z6'; //it can be taken also from environment variables
  private readonly perPage: number = 25;

  constructor(private httpClient: HttpClient) { }

  public getImages(term: string, page: number = 0): Observable<any> {
    const offset = this.perPage * page;
    return this.httpClient.get(
      `https://api.giphy.com/v1/gifs/search?api_key=${this.API_KEY}&q=${term}&offset=${offset}`,
      {observe: 'body'});
  }
}

