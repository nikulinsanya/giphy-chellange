import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import Image from '../../models/Image';
import Giphy from '../../models/Giphy';
import Pagination from '../../models/Pagination';


@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {
  public searchForm: FormGroup;
  public images: Array<Image> = [];
  public currentPage: Array<Image> = [];
  public pagination: Pagination;
  constructor(private apiService: ApiService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      term: ['']
    });
  }

  onSubmit() {
    const { term } = this.searchForm.value;
    this.requestImages(term);
  }

  requestImages(term?: string, page?: number) {
    this.apiService.getImages(term, page).subscribe((data: Giphy) => {
      this.images = data.data;
      this.pagination = data.pagination;
    });
  }

  pageChange(page: number) {
    const { term } = this.searchForm.value;
    this.requestImages(term, page);
  }

}
