import {Component, Input, OnInit} from '@angular/core';
import Image from '../../../../models/Image';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {
  @Input() image: Image;

  constructor() { }

}
