import {Component, Input, OnChanges, Output, SimpleChanges, EventEmitter} from '@angular/core';
import Pagination from '../../../../models/Pagination';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss']
})
export class PagingComponent implements OnChanges {
  @Input() pagination: Pagination = new Pagination();
  @Output() onPageChange = new EventEmitter();
  public pages: number;
  public currentPage: number;
  public counter = Array;
  private readonly perPage: number = 25;

  constructor() {
  }

  setPage(page): void {
    this.onPageChange.emit(page);
  }

  ngOnChanges(changes: SimpleChanges) {
    const pagination = changes.pagination.currentValue;
    if (pagination) {
      this.pages = Math.round(pagination.total_count / this.perPage);
      this.currentPage = pagination.offset / this.perPage;
    }
  }

}
