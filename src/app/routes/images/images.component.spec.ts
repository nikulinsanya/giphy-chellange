import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ImagesComponent} from './images.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ImageComponent} from './components/image/image.component';
import {PagingComponent} from './components/paging/paging.component';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from '../../services/api.service';
import {Observable, of} from 'rxjs';
import Giphy from '../../models/Giphy';
import Pagination from '../../models/Pagination';

describe('Main component: ImagesComponent', () => {
  let component: ImagesComponent;
  let fixture: ComponentFixture<ImagesComponent>;
  let apiService: ApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImagesComponent, ImageComponent, PagingComponent],
      imports: [ReactiveFormsModule, FormsModule, HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesComponent);
    component = fixture.componentInstance;
    apiService = fixture.debugElement.injector.get(ApiService);
    fixture.detectChanges();
  });

  it('should create component with dependencies', () => {
    expect(component).toBeTruthy();
  });

  it('should be available to be called \'getImages\'', async(() => {
    spyOn(apiService, 'getImages').and.callThrough();
    apiService.getImages('term');
    expect(apiService.getImages).toHaveBeenCalled();
  }));

  it('shouldn\'t  \'getImages\'  be called onInit', async(() => {
    fixture.detectChanges();
    spyOn(apiService, 'getImages').and.callThrough();
    expect(apiService.getImages).not.toHaveBeenCalled();
  }));

  it('should fetch data via \'getImages\' if \'page change\' is called', async(() => {
    let app = fixture.debugElement.componentInstance;
    const data: Giphy = new Giphy();
    apiService.getImages = (term) => of(data);
    fixture.detectChanges();
    spyOn(apiService, 'getImages').and.callThrough();
    app.pageChange('term');
    expect(apiService.getImages).toHaveBeenCalled();
  }));

  it('should subscribe to data if \'getImages\' is called', async(() => {
    const data: Giphy = new Giphy();
    apiService.getImages = (term) => of(data);
    fixture.detectChanges();
    spyOn(apiService, 'getImages').and.callThrough();
    apiService.getImages('term').subscribe((res: Giphy) => {
      expect(res).toEqual(data);
    });
  }));
});
