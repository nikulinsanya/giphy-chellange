import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project GiphyChallenge', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display \'Search for gifs\' message on the page load', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Search for gifs');
  });

  it('should display search field', () => {
    page.navigateTo();
    expect(page.getInputField().isPresent()).toBe(true);
  });

  it('should display search button', () => {
    page.navigateTo();
    expect(page.getSearchButton().isPresent()).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
