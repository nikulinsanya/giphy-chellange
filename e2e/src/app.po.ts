import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h3')).getText() as Promise<string>;
  }
  getInputField() {
    return element(by.css('app-root input[type="text"]'));
  }
  getSearchButton() {
    return element(by.css('app-root button[type="submit"]'));
  }
}
